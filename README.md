# mando\_\_fe-test

> A frontend dev test

To read the original test tasks, please [go to Github](https://github.com/andypimlett/frontend-test).

## Test notes

- Quick scaffold project using @vue/cli
- I'd usually add eslint config, but used global eslint instead
- I copied some style and vue snippets from my personal site for speed
- Look for TODO comments to see what i was moving onto next
- Ran out of time for the form validation and sumbission
- Didn't do XHR as v-loops seemed quicker (but can use and have done in past projects)
- Course Cards CSS needs work (vertical-spacing) - with a do-over I'd use Grid not flex for that
- In retrospect, plain HTML-css-js would have been simpler, but Vue is lovely to work with and offers more scope for further development of components :)
- Total time spent: **2hours, 20mins**

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
